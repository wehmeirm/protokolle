# Sammlung aller Protokolle der Fachschaftsinitiative MaPhIn

Die Protokolle werden in Markdown geschrieben. Wie das geht steht [hier](https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf).

Für neue Protokolle bitte die VORLAGE.md kopieren und mit dem Datum¹ der Sitzung benennen.

Für jede Art von Sitzungen sollte es einen eigenen Ordner geben. Bisher gibt es nur **FS-Treffen**

## Was tun bei Fehlern im Protokoll?

Kleinere Fehler, insbesondere Rechtschreibfehler können direkt verbessert werden und von Mitgliedern des Projekts gepusht werden.

Für inhaltliche Fehler und Verbesserungen bitte ein [neues Issue](https://gitlab.informatik.hu-berlin.de/fsini-maphin/protokolle/-/issues/new) öffnen. 


¹) Datum im ISO-8601 Format!
![xkcd ISO 8601 Datumsformat](https://imgs.xkcd.com/comics/iso_8601.png)
[CC BY-NC 2.5 xkcd](http://creativecommons.org/licenses/by-nc/2.5/)
